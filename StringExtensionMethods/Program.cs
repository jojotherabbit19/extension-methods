﻿string str = " hello,    world! ";

Console.WriteLine($"Number of words: {str.WordCount()}");
Console.WriteLine($"After remove white space: {str.RemoveWhiteSpace()}");
Console.WriteLine($"Uppercase: {str.UppercaseFirstLetter()}");


static class StringExtensions
{
    public static int WordCount(this string str)
    {
        // spit each words
        string[] words = str.Split(" ");

        // select words are not null or whiteSpace
        words = words.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();

        // return wordCount
        return words.Count();
    }
    public static string RemoveWhiteSpace(this string str) => str.Trim().Replace(" ", string.Empty);
    public static string UppercaseFirstLetter(this string str)
    {
        // spit each words
        string[] words = str.Split(" ");

        // select words are not null or whiteSpace
        words = words.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();

        // upperCase each word after spit
        for (int i = 0; i < words.Length; i++)
        {
            words[i] = char.ToUpper(words[i][0]) + words[i].Substring(1);
        }

        // join string[] => string
        return string.Join(" ", words);
    }
}
