﻿// student array
Student[] studentArr =
{
    new Student("Dung", 17, 10),
    new Student("Dep", 17, 5),
    new Student("Trai", 17, 4),
    new Student("Yeu", 17, 3),
    new Student("Link", 17, 2),
    new Student("Ka", 17, 1),
    new Student("Va", 17, 0),
    new Student("Dua Lipa", 17, 5),
};

foreach (var item in studentArr)
{
    var letterGrade = item.GetLetterGrade().ToString();
    var isPassing = item.IsPassing().ToString();
    Console.WriteLine("");
    Console.WriteLine($"Name: {item.Name} | Age: {item.Age} | GPA: {item.GPA}");
    Console.WriteLine($"Letter grade: {letterGrade}");
    Console.WriteLine($"Status: {isPassing}");
}



class Student
{
    public string Name { get; set; }
    public int Age { get; set; }
    public double GPA { get; set; }
    public Student(string name, int age, double gPA)
    {
        Name = name;
        Age = age;
        GPA = gPA;
    }
}

enum Grade
{
    A,
    B,
    C,
    D,
    F
}

static class StudentExtensions
{
    public static Grade GetLetterGrade(this Student student)
    {
        return student.GPA switch
        {
            >= 4.0 and <= 5.0 => Grade.A,
            >= 3.0 and < 4.0 => Grade.B,
            >= 2.0 and < 3.0 => Grade.C,
            >= 1.0 and < 2.0 => Grade.D,
            _ => Grade.F
        };
    }
    public static bool IsPassing(this Student student)
    {
        if (student.GPA > 2.0)
        {
            return true;
        }

        return false;
    }
}